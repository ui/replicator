import os
import importlib
import logging

import yaml

from replicator.utils import timed, profile

logger = logging.getLogger(__name__)


class Replicator:
    def __init__(self, source, target, commissioning):
        self.source = source
        self.target = target
        self.commissioning = commissioning

    def run(self, beamlines, start, end, add_defaults=False):
        self.source.retrieve(beamlines, start, end)
        self.commissioning.set_dates(beamlines, start, end)

        self.process_persons()
        self.process_proposals(
            add_default_person=add_defaults, commissioning=self.commissioning.proposals
        )
        self.process_sessions(
            add_default_shp=add_defaults, commissioning=self.commissioning.proposals
        )
        self.process_session_has_persons()
        self.process_components()

        self.commissioning.create()

        profile()

    @timed
    def process_persons(self):
        target_person_count, target_persons = self.target.get_persons()

        to_add = []
        for src in self.source.get_persons():
            for tar in target_persons:
                if tar["externalid"] == src["externalid"] or (
                    tar["login"] is not None and tar["login"] == src["login"]
                ):
                    update = False
                    for k in ["givenname", "familyname", "externalid"]:
                        if tar[k] != src[k]:
                            if k == "externalid":
                                logger.warning(
                                    f"Externalid has changed for {src['login']} from {tar['externalid']} to {src['externalid']}"
                                )
                            update = True

                    if update:
                        self.target.update_person(tar["personid"], src)

                    break
            else:
                to_add.append(src)

        self.target.bulk_add("persons", to_add)

    @timed
    def process_proposals(self, add_default_person=False, commissioning=[]):
        target_proposal_count, target_proposals = self.target.get_proposals()
        to_add = []
        for src in self.source.get_proposals():
            for tar in target_proposals:
                if tar["externalid"] == src["externalid"] or (
                    tar["proposalcode"] == src["proposalcode"]
                    and tar["proposalnumber"] == src["proposalnumber"]
                ):
                    update = False
                    for k in ["title", "proposalcode"]:
                        if tar[k] != src[k]:
                            update = True

                    if update:
                        self.target.update_proposal(tar["proposalid"], src)

                    break
            else:
                to_add.append(src)

        self.target.bulk_add("proposals", to_add)

        if add_default_person:
            target_person_count, target_persons = self.target.get_persons()

            persons = []
            for prop in to_add:
                if prop["proposalcode"] in commissioning:
                    logger.info(
                        f"Proposal {prop['proposalcode']}{prop['proposalnumber']} is a commissioning proposal, not adding default person"
                    )
                    continue

                login = f"{prop['proposalcode']}{prop['proposalnumber']}"
                exists = False
                for p in target_persons:
                    if login == p["login"]:
                        exists = True

                if not exists:
                    persons.append(
                        {
                            "login": login,
                            "givenname": prop["proposalcode"],
                            "familyname": prop["proposalnumber"],
                        }
                    )

            self.target.bulk_add("persons", persons)

    @timed
    def process_sessions(self, add_default_shp=False, commissioning=[]):
        target_session_count, target_sessions = self.target.get_sessions()
        to_add = []
        for src in self.source.get_sessions():
            for tar in target_sessions:
                if tar["externalid"] == src["externalid"]:
                    update = False
                    for k in ["startdate", "enddate"]:
                        if tar[k] != src[k]:
                            update = True

                    if update:
                        self.target.update_session(tar["sessionid"], src)

                    break
            else:
                to_add.append(src)

        self.target.bulk_add("sessions", to_add)

        if add_default_shp:
            target_person_count, target_persons = self.target.get_persons()
            target_shp_count, target_shp = self.target.get_session_has_persons()

            shps = []
            for session in to_add:
                if session["proposalcode"] in commissioning:
                    logger.info(
                        f"Session {session['proposal']} is a commissioning proposal, not adding default session_has_person"
                    )
                    continue

                person = None
                for p in target_persons:
                    if p["login"] == session["proposal"]:
                        person = p

                if person:
                    exists = False
                    for shp in target_shp:
                        if (
                            shp["sexternalid"] == session["externalid"]
                            and shp["personid"] == person["personid"]
                        ):
                            exists = True

                    if not exists:
                        shps.append(
                            {
                                "personid": person["personid"],
                                "session": session["externalid"],
                                "role": "Co-Investigator",
                            }
                        )

            self.target.bulk_add("shps", shps)

    @timed
    def process_session_has_persons(self):
        target_shp_count, target_shp = self.target.get_session_has_persons()
        to_add = []
        for src in self.source.get_session_has_persons():
            for tar in target_shp:
                if (
                    tar["sexternalid"] == src["session"]
                    and tar["pexternalid"] == src["person"]
                ):
                    if tar["role"] != src["role"]:
                        self.target.update_session_has_person(
                            tar["sessionid"], tar["personid"], src["role"]
                        )
                    break
            else:
                to_add.append(src)

        self.target.bulk_add("shps", to_add)

    @timed
    def process_components(self):
        target_components_count, target_components = self.target.get_components()
        to_add = []
        for src in self.source.get_components():
            for tar in target_components:
                if tar["externalid"] == src["externalid"]:
                    update = False
                    for k in ["name", "acronym"]:
                        if tar[k] != src[k]:
                            update = True

                    if update:
                        self.target.update_component(tar["componentid"], src)

                    break
            else:
                to_add.append(src)

        self.target.bulk_add("components", to_add)


def loader(cls, cls_kwargs, **kwargs):
    mod = importlib.import_module(
        f"replicator.impl.{kwargs.get('module', '')}{cls.lower()}"
    )
    return getattr(mod, cls)(**cls_kwargs)


def config(conf="config.yml"):
    if os.path.exists(conf):
        with open(conf, "r") as stream:
            try:
                return yaml.safe_load(stream)
            except yaml.YAMLError:
                print(f"{conf} is malformed")
                exit()
    else:
        print(f"Cant read {conf}")
        exit()
