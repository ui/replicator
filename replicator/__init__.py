# -*- coding: utf-8 -*-
"""replicator main package
"""
from . import release

__version__ = release.version
__author__ = release.author
__license__ = release.license
version_info = release.version_info
