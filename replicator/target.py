import logging
from abc import abstractmethod

logger = logging.getLogger(__name__)


class Target:
    def clean(self):
        raise NotImplementedError

    @abstractmethod
    def bulk_add(self, type, rows):
        pass

    @abstractmethod
    def update_component(self, componentid, component):
        pass

    @abstractmethod
    def update_person(self, personid, person):
        pass

    @abstractmethod
    def update_proposal(self, proposalid, proposal):
        pass

    @abstractmethod
    def update_session(self, sessionid, session):
        pass

    @abstractmethod
    def update_session_has_person(self, sessionid, personid, role):
        pass
