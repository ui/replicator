# -*- coding: utf-8 -*-

# Allows running the server with "python -m replicator"
from replicator.replicate import main

main()
