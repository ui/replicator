import logging
from abc import ABC, abstractmethod

logger = logging.getLogger(__name__)


class Commissioning(ABC):
    _beamlines = []

    _start = None
    _end = None

    def __init__(self, source, target):
        self.source = source
        self.target = target

    def set_dates(self, beamlines, start, end):
        self._beamlines = beamlines
        self._start = start
        self._end = end

    def set_target(self, target):
        self.target = target

    @abstractmethod
    def clean(self, beamlines):
        pass

    @abstractmethod
    def create(self):
        pass

    @property
    @abstractmethod
    def proposals(self):
        pass
