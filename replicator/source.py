import logging
from abc import abstractmethod

logger = logging.getLogger(__name__)


class Source:
    def retrieve(self, beamlines, start, end):
        logger.warning("Source does not implement retrieve")

    @abstractmethod
    def get_components(self):
        pass

    @abstractmethod
    def get_proposals(self):
        pass

    @abstractmethod
    def get_sessions(self):
        pass

    @abstractmethod
    def get_persons(self):
        pass

    @abstractmethod
    def get_session_has_persons(self):
        pass
