import time
import logging
from functools import wraps

from tabulate import tabulate

logger = logging.getLogger(__name__)


_calls = []
_counted = {}


def profile():
    took = sum([i[2] for i in _calls])
    _calls.append(["", "Total", took])

    table = tabulate(_calls, headers=["Class", "Function", "Took(s)"])
    logger.info(
        f"\n{table}", extra={"replicator_run": 1, "replicator_execution_time": took}
    )

    counted = []
    calls = 0
    for n, count in _counted.items():
        cls, fn = n.split(".")
        calls += count
        counted.append([cls, fn, count])

    table2 = tabulate(counted, headers=["Class", "Function", "Inserts/Updates"])
    logger.info(f"\n{table2}", extra={"replicator_calls": calls})


def timed(fn):
    @wraps(fn)
    def wrapper(self, *args, **kwargs):
        start = time.time()
        result = fn(self, *args, **kwargs)
        took = round(time.time() - start, 3)
        _calls.append([self.__class__.__name__, fn.__name__, took])

        return result

    return wrapper


def counted(fn):
    @wraps(fn)
    def wrapped(self, *args, **kwargs):
        name = f"{self.__class__.__name__}.{fn.__name__}"

        if not (name in _counted):
            _counted[name] = 0

        _counted[name] += 1
        return fn(self, *args, **kwargs)

    return wrapped


def counted_bulk(fn):
    @wraps(fn)
    def wrapped(self, type, rows):
        if f"{self.__class__.__name__}.bulk_add_{type}" not in _counted:
            _counted[f"{self.__class__.__name__}.bulk_add_{type}"] = 0

        _counted[f"{self.__class__.__name__}.bulk_add_{type}"] += len(rows)
        return fn(self, type, rows)

    return wrapped
