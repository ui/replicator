import copy
import logging
from contextlib import contextmanager

from sqlalchemy import (
    orm,
    event,
    create_engine,
    func,
    Integer,
    String,
    TypeDecorator,
    and_,
)
from sqlalchemy.schema import Table, MetaData
from sqlalchemy.sql.expression import cast
from sqlalchemy.ext.declarative import declarative_base

from replicator.source import Source
from replicator.target import Target
from replicator.utils import counted, counted_bulk

logger = logging.getLogger(__name__)


class format_dict(dict):
    def __getitem__(self, key):
        try:
            return super().__getitem__(key)
        except KeyError:
            return "{" + str(key) + "}"


class MixedBinary(TypeDecorator):
    impl = String
    cache_ok = False

    def process_result_value(self, value, dialect):
        if isinstance(value, str):
            value = bytes(value, "utf-8")
        elif value is not None:
            value = bytes(value)

        return value


@event.listens_for(Table, "column_reflect")
def column_reflect(inspector, table, column_info):
    column_info["key"] = column_info["name"].lower()

    # TODO: mysqlconnector helpfully tries to convert binary to strings
    #   (sigh) convert it back again...
    if column_info["key"] == "externalid":
        column_info["type"] = MixedBinary(16)


def unique(_list):
    return list(set(_list))


class ISPYB(Source, Target):
    def __init__(self, *args, **kwargs):
        self._dry_run = kwargs.get("dry_run")

        char = ""
        if kwargs.get("charset"):
            char = f"?charset={kwargs['charset']}"

        self._engine = create_engine(
            f"mysql+mysqlconnector://{kwargs['user']}:{kwargs['pass']}@{kwargs['url']}{char}",
            isolation_level="READ UNCOMMITTED",
            pool_pre_ping=True,
            pool_recycle=3600,
            connect_args={"use_pure": True},
        )

        self._connection = self._engine.connect()
        self._metadata = MetaData(bind=self._engine)

        self._session_maker = orm.sessionmaker()
        self._session_maker.configure(bind=self._engine)

        for t in [
            "Person",
            "Proposal",
            "BLSession",
            "Session_has_Person",
            "Protein",
            "Permission",
            "UserGroup",
            "UserGroup_has_Permission",
            "UserGroup_has_Person",
        ]:
            table = type(
                t,
                (declarative_base(),),
                {"__table__": Table(t, self._metadata, autoload=True)},
            )

            setattr(self, t, table)

        self._next_visit_number = {}

    @contextmanager
    def session_scope(self):
        session = self._session_maker()
        try:
            yield session
            session.commit()
        except Exception:
            session.rollback()
            raise
        finally:
            session.close()

    def decode(self, column):
        return cast(func.CONV(func.HEX(column), 16, 10), Integer)

    def clean(self):
        proposal_count, proposals = self.get_proposals()

        with self.session_scope() as ses:
            logins = [p["proposalcode"] + str(p["proposalnumber"]) for p in proposals]
            for login in ses.query(self.Person).filter(self.Person.login.in_(logins)):
                ses.delete(login)

            ses.query(self.BLSession).filter(
                self.BLSession.externalid != None  # noqa: E711
            ).delete()
            ses.query(self.Proposal).filter(
                self.Proposal.externalid != None  # noqa: E711
            ).delete()
            ses.query(self.Person).filter(
                self.Person.externalid != None  # noqa: E711
            ).delete()

            ses.commit()

    def get_proposals(self):
        with self.session_scope() as ses:
            proposals = ses.query(
                self.Proposal.proposalid,
                self.Proposal.title,
                self.Proposal.proposalcode,
                self.Proposal.proposalnumber,
                self.decode(self.Proposal.externalid).label("externalid"),
            )

            return proposals.count(), [p._asdict() for p in proposals.all()]

    def get_sessions(self):
        with self.session_scope() as ses:
            sessions = ses.query(
                self.BLSession.sessionid,
                self.BLSession.startdate,
                self.BLSession.enddate,
                self.BLSession.visit_number,
                self.BLSession.proposalid,
                self.Proposal.proposalcode,
                self.Proposal.proposalnumber,
                self.decode(self.BLSession.externalid).label("externalid"),
            ).join(self.Proposal)

            count = sessions.count()
            sessions = sessions.all()

            for s in sessions:
                if not (s.proposalid in self._next_visit_number):
                    self._next_visit_number[s.proposalid] = 0

                if s.visit_number > self._next_visit_number[s.proposalid]:
                    self._next_visit_number[s.proposalid] = s.visit_number

            return count, [s._asdict() for s in sessions]

    def get_persons(self):
        with self.session_scope() as ses:
            persons = ses.query(
                self.Person.personid,
                self.Person.givenname,
                self.Person.familyname,
                self.Person.login,
                self.decode(self.Person.externalid).label("externalid"),
            )

            return persons.count(), [p._asdict() for p in persons.all()]

    def get_session_has_persons(self):
        with self.session_scope() as ses:
            shp = (
                ses.query(
                    self.Session_has_Person.personid,
                    self.Session_has_Person.sessionid,
                    self.Session_has_Person.role,
                    self.decode(self.BLSession.externalid).label("sexternalid"),
                    self.decode(self.Person.externalid).label("pexternalid"),
                    func.concat(
                        self.Proposal.proposalcode, self.Proposal.proposalnumber
                    ).label("proposal"),
                )
                .join(
                    self.BLSession,
                    self.BLSession.sessionid == self.Session_has_Person.sessionid,
                )
                .join(
                    self.Person,
                    self.Session_has_Person.personid == self.Person.personid,
                )
                .join(
                    self.Proposal, self.BLSession.proposalid == self.Proposal.proposalid
                )
            )

            return shp.count(), [shp._asdict() for shp in shp.all()]

    def get_components(self):
        with self.session_scope() as ses:
            proteins = ses.query(
                self.Protein.proteinid.label("componentid"),
                self.Protein.acronym,
                self.Protein.name,
                self.decode(self.Protein.externalid).label("externalid"),
            )

            return proteins.count(), [p._asdict() for p in proteins.all()]

    def find_proposalids(self, proposals):
        with self.session_scope() as ses:
            proposalids = (
                ses.query(
                    self.Proposal.proposalid,
                    func.concat(
                        self.Proposal.proposalcode, self.Proposal.proposalnumber
                    ).label("proposal"),
                )
                .filter(
                    func.concat(
                        self.Proposal.proposalcode, self.Proposal.proposalnumber
                    ).in_(proposals)
                )
                .all()
            )

            return {p.proposal: p.proposalid for p in proposalids}

    def find_personids(self, externalids):
        with self.session_scope() as ses:
            personids = ses.query(
                self.Person.personid,
                self.decode(self.Person.externalid).label("externalid"),
            ).filter(self.decode(self.Person.externalid).in_(externalids))

            return {p.externalid: p.personid for p in personids.all()}

    def find_sessionids(self, externalids):
        with self.session_scope() as ses:
            sessionids = (
                ses.query(
                    self.BLSession.sessionid,
                    self.decode(self.BLSession.externalid).label("externalid"),
                )
                .filter(self.decode(self.BLSession.externalid).in_(externalids))
                .all()
            )

            return {p.externalid: p.sessionid for p in sessionids}

    @counted_bulk
    def bulk_add(self, type, input_rows):
        rows = copy.deepcopy(input_rows)
        # print(rows)
        types = {
            "proposals": self.Proposal,
            "sessions": self.BLSession,
            "persons": self.Person,
            "shps": self.Session_has_Person,
            "components": self.Protein,
            "ughps": self.UserGroup_has_Person,
        }

        if not (type in types):
            raise KeyError(f"No such bulk insert type {type}")

        if type == "sessions" or type == "components":
            proposalids = self.find_proposalids(unique([r["proposal"] for r in rows]))
            invalid_rows = []
            for r in rows:
                if not r.get("proposalid"):
                    try:
                        r["proposalid"] = proposalids[r["proposal"]]
                        del r["proposal"]
                    except KeyError:
                        logger.error(f"Could not find a proposalid for {r['proposal']}")
                        invalid_rows.append(r)

            for r in invalid_rows:
                logger.error(f"Not processing invalid row: {r}")
                rows.remove(r)

        if type == "sessions":
            for r in rows:
                if not (r["proposalid"] in self._next_visit_number):
                    self._next_visit_number[r["proposalid"]] = 0

                self._next_visit_number[r["proposalid"]] += 1
                r["visit_number"] = self._next_visit_number[r["proposalid"]]

        if type == "proposals" or type == "shps":
            personids = self.find_personids(unique([r.get("person") for r in rows]))
            for r in rows:
                if not r.get("personid"):
                    try:
                        r["personid"] = personids[r["person"]]
                    except KeyError:
                        logger.error(
                            f"Could not find a personid for externalid {r['person']}"
                        )

                    del r["person"]

        if type == "shps":
            sessionids = self.find_sessionids(unique([r["session"] for r in rows]))
            for r in rows:
                if not r.get("sessionid"):
                    try:
                        r["sessionid"] = sessionids[r["session"]]
                    except KeyError:
                        logger.error(
                            f"Could not find a sessionid for externalid {r['session']}"
                        )

                del r["session"]

        with self.session_scope() as ses:
            log_types = {
                "persons": "person %(givenname)s %(familyname)s [%(login)s]",
                "proposals": "proposal %(proposalcode)s%(proposalnumber)s [personid %(personid)s]",
                "sessions": "session proposalid %(proposalid)s visit %(visit_number)s on %(beamlinename)s",
                "components": "component proposalid %(proposalid)s %(acronym)s",
                "shps": "session_has_person personid %(personid)s sessionid %(sessionid)s role %(role)s",
            }

            for r in rows:
                logger.info(
                    "Inserting " + log_types[type],
                    format_dict(r),
                    extra={"ispyb_" + key: val for key, val in r.items()},
                )
                if "externalid" in r:
                    r["externalid"] = r["externalid"].to_bytes(16, byteorder="big")

            if not self._dry_run:
                ses.bulk_insert_mappings(types[type], rows)

    def get_and_ensure_group(self, group):
        with self.session_scope() as ses:
            group = (
                ses.query(self.UserGroup).filter(self.UserGroup.name == group).first()
            )
            if group:
                return group.usergroupid

            group = self.UserGroup(name=group)
            ses.add(group)
            ses.commit()

            permission = self.Permission(name=group)
            ses.add(permission)
            ses.commit()

            uhp = self.UserGroup_has_Permission(
                usergroupid=group.usergroupid, permissionid=permission.permissionid
            )
            ses.add(uhp)
            ses.commit()

            return group.usergroupid

    @counted
    def update_person(self, personid, person):
        logger.info(f"Updating {personid} with {person}")
        with self.session_scope() as ses:
            pers = (
                ses.query(self.Person).filter(self.Person.personid == personid).first()
            )
            if pers:
                pers.externalid = person["externalid"].to_bytes(16, byteorder="big")
                ses.commit()
            else:
                logger.warning(f"Could not find person {personid}")

    @counted
    def update_proposal(self, proposalid, proposal):
        logger.info(f"Updating {proposalid} with {proposal}")

    @counted
    def update_session(self, sessionid, session):
        logger.info(
            f"Updating {sessionid} with {session['startdate']} {session['enddate']}"
        )
        with self.session_scope() as ses:
            sess = (
                ses.query(self.BLSession)
                .filter(self.BLSession.sessionid == sessionid)
                .first()
            )
            if sess:
                sess.startdate = session["startdate"]
                sess.enddate = session["enddate"]
                ses.commit()
            else:
                logger.warning(f"Could not find session {sessionid}")

    @counted
    def update_session_has_person(self, sessionid, personid, role):
        logger.info(f"Updating {sessionid} {personid} with role {role}")
        with self.session_scope() as ses:
            shp = (
                ses.query(self.Session_has_Person)
                .filter(
                    and_(
                        self.Session_has_Person.sessionid == sessionid,
                        self.Session_has_Person.personid == personid,
                    )
                )
                .first()
            )
            if shp:
                shp.role = role
                ses.commit()
            else:
                logger.warning(
                    f"Could not find shp for sessionid {sessionid} and personid {personid}"
                )
