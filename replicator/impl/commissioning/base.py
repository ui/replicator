import logging

from replicator.commissioning import Commissioning

logger = logging.getLogger(__name__)


class Base(Commissioning):
    def __init__(self, *args, **kwargs):
        self._proposalcodes = kwargs.get("proposalcodes", [])

    def clean(self, beamlines):
        pass

    def create(self):
        pass

    @property
    def proposals(self):
        return self._proposalcodes
