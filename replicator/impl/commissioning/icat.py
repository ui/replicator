import logging
import pprint

from datetime import datetime
from dateutil.relativedelta import relativedelta

import pytz

from replicator.utils import timed
from replicator.commissioning import Commissioning

logger = logging.getLogger(__name__)

pp = pprint.PrettyPrinter(indent=4)

eu = pytz.timezone("Europe/Paris")
utc = pytz.timezone("UTC")


def to_int(string):
    string = string.lower().replace("id", "1")
    string = string.lower().replace("bm", "2")

    try:
        return int(string)
    except ValueError:
        logger.warning(f"Could not convert {string} to int")
        return 0


class ICAT(Commissioning):
    def __init__(self, *args, **kwargs):
        self._proposalcodes = kwargs.get("proposalcodes", [])

    def clean(self, beamlines):
        pass

    @timed
    def create(self):
        target_proposal_count, target_proposals = self.target.get_proposals()

        proposals = []
        for bl in self._beamlines:
            prop_count = (self._end.year - self._start.year) * 12 + (
                self._end.month - self._start.month
            )

            for i in range(prop_count):
                date = self._start + relativedelta(months=i)
                ym = date.strftime("%y%m")
                proposals.append(
                    {
                        "proposalcode": bl,
                        "proposalnumber": ym,
                        "externalid": to_int(bl + ym),
                        "title": f"{bl.upper()} Local beamline commissioning {date.strftime('%b %Y')}",
                    }
                )

        to_add = []
        to_add_sessions = []
        to_add_persons = []
        for src in proposals:
            for tar in target_proposals:
                if (
                    tar["proposalcode"] == src["proposalcode"]
                    and tar["proposalnumber"] == src["proposalnumber"]
                ):
                    break
            else:
                to_add_persons.append(
                    {
                        "givenname": src["proposalcode"],
                        "familyname": src["proposalnumber"],
                        "externalid": src["externalid"],
                    }
                )
                src["person"] = src["externalid"]
                to_add.append(src)

        target_session_count, target_sessions = self.target.get_sessions()
        for src in to_add:
            for tar in target_sessions:
                if (
                    tar["proposalcode"] == src["proposalcode"]
                    and tar["proposalnumber"] == src["proposalnumber"]
                ):
                    break

            else:
                year = int(src["proposalnumber"][0:2]) + 2000
                month = int(src["proposalnumber"][2:])
                start = datetime(year, month, 1, 0, 0, 1)
                end = datetime(year, month, 1, 23, 59, 59) + relativedelta(day=31)
                to_add_sessions.append(
                    {
                        "externalid": to_int(
                            str(src["externalid"]) + start.strftime("%y%m%d")
                        ),
                        "beamlinename": src["proposalcode"].upper(),
                        "proposal": src["proposalcode"] + src["proposalnumber"],
                        "startdate": start,
                        "enddate": end,
                        "scheduled": 0,
                    }
                )

        self.target.bulk_add("persons", to_add_persons)
        self.target.bulk_add("proposals", to_add)
        self.target.bulk_add("sessions", to_add_sessions)

    @property
    def proposals(self):
        return [bl for bl in self._beamlines] + self._proposalcodes
