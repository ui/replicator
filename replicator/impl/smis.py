import logging
from requests import Session
from requests.auth import HTTPBasicAuth

import pytz

from zeep import Client
from zeep.transports import Transport

from unidecode import unidecode

from replicator.utils import timed
from replicator.source import Source

logger = logging.getLogger(__name__)

eu = pytz.timezone("Europe/Paris")
utc = pytz.timezone("UTC")


class SMIS(Source):
    roles = {
        "Local Contact",
        "Local Contact 2",
        "Staff",
        "Team Leader",
        "Co-Investigator",
        "Principal Investigator",
        "Alternate Contact",
    }

    proposals = {}
    sessions = {}
    persons = {}
    session_has_persons = {}
    components = {}

    def __init__(self, **kwargs):
        self._config = kwargs
        self.session = Session()
        self.session.auth = HTTPBasicAuth(kwargs["user"], kwargs["pass"])

        self.client = Client(
            kwargs["url"],
            transport=Transport(session=self.session),
        )

    @timed
    def retrieve(self, beamlines, start, end):
        for b in beamlines:
            resp = self.client.service.findSessionsByBeamlineAndDates(b, start, end)
            for i, s in enumerate(resp):
                proposalcode = s.categCode.lower().replace("-", "")
                proposal = f"{proposalcode}{s.categCounter}"
                self.proposals[s.proposalPk] = {
                    "externalid": s.proposalPk,
                    "title": unidecode(s.proposalTitle),
                    "person": s.mainProposer.scientistPk,
                    "proposalcode": proposalcode,
                    "proposalnumber": s.categCounter,
                }
                self.sessions[s.pk] = {
                    "proposal": proposal,
                    "proposalcode": proposalcode,
                    "externalid": s.pk,
                    "startdate": s.startDate.astimezone(eu).replace(tzinfo=None),
                    "enddate": s.endDate.astimezone(eu).replace(tzinfo=None),
                    "beamlinename": s.beamlineName,
                    "nbshifts": s.shifts,
                    "comments": s.comment,
                    "scheduled": 1,
                    "beamlineoperator": ", ".join(
                        [f"{p.firstName} {p.realName}" for p in s.localContacts]
                    )[:44],
                }

                for p in s.localContacts + [s.mainProposer] + s.participants:
                    login = p.userName if hasattr(p, "userName") else None

                    if login or p == s.mainProposer:
                        self.persons[p.scientistPk] = {
                            "externalid": p.scientistPk,
                            "givenname": unidecode(p.firstName),
                            "familyname": unidecode(p.realName),
                            "login": login,
                        }

                    shp_key = f"{s.pk}-{p.scientistPk}"
                    if login and shp_key not in self.session_has_persons:
                        self.session_has_persons[shp_key] = {
                            "session": s.pk,
                            "person": p.scientistPk,
                            "role": "Local Contact"
                            if p in s.localContacts
                            else (
                                "Principal Investigator"
                                if p.scientistPk == s.mainProposer.scientistPk
                                else "Co-Investigator"
                            ),
                        }

    def get_components(self):
        for pk in self.proposals.keys():
            resp = self.client.service.findSamplesheetInfoLightForProposalPk(
                pk, self._config.get("validated", False)
            )
            for r in resp:
                proposalcode = r.categoryCode.lower().replace("-", "")
                self.components[r.pk] = {
                    "proposal": f"{proposalcode}{r.categoryCounter}",
                    "acronym": r.acronym,
                    "name": r.acronym,
                    "description": unidecode(r.description),
                    "externalid": r.pk,
                }

        return self.components.values()

    def get_proposals(self):
        return self.proposals.values()

    def get_sessions(self):
        return self.sessions.values()

    def get_persons(self):
        return self.persons.values()

    def get_session_has_persons(self):
        return self.session_has_persons.values()
