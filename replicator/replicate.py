import os
import argparse
import logging
from datetime import datetime, timedelta

import replicator
from replicator.base import Replicator, loader, config as loadConfig

logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)


class GrayPyFilter(logging.Filter):
    """Inject some useful metadata into graylog records"""

    def filter(self, record):
        record.replicator_version = replicator.__version__
        record.process_name = "replicator"

        return True


def init_graylog(config):
    if config.get("graylog_host") and config.get("graylog_port"):
        import graypy

        class PythonLevelToSyslogConverter:
            @staticmethod
            def get(level, _):
                if level < 20:
                    return 7  # DEBUG
                elif level < 25:
                    return 6  # INFO
                elif level < 30:
                    return 5  # NOTICE
                elif level < 40:
                    return 4  # WARNING
                elif level < 50:
                    return 3  # ERROR
                elif level < 60:
                    return 2  # CRITICAL
                else:
                    return 1  # ALERT

        graypy.handler.SYSLOG_LEVELS = PythonLevelToSyslogConverter()

        handler = graypy.GELFUDPHandler
        graylog = handler(
            config["graylog_host"], config["graylog_port"], level_names=True
        )
        graylog.addFilter(GrayPyFilter(config))
        logging.getLogger().addHandler(graylog)


def main(argv=None):
    parser = argparse.ArgumentParser(description="replicator")

    parser.add_argument(
        "-s",
        "--start",
        dest="start",
        default=5,
        help="# weeks back to start",
        type=int,
    )

    parser.add_argument(
        "-e", "--end", dest="end", default=5, help="# weeks forward to end", type=int
    )

    parser.add_argument(
        "-c",
        "--clean",
        action="store_true",
        dest="clean",
        help="(!) remove any replicated data",
    )

    parser.add_argument(
        "-d",
        "--dry-run",
        action="store_true",
        dest="dry_run",
        help="Dry run, show changes but dont update database",
    )

    args = parser.parse_args(args=argv)

    config = loadConfig(os.environ.get("REPLICATOR_CONFIG", "config.yml"))
    init_graylog(config)

    for ty in ["source", "target"]:
        for k in ["user", "pass", "url"]:
            key = f"{ty.upper()}_{k.upper()}"
            if os.environ.get(key):
                config[ty]["config"][k] = os.environ[key]

    if args.dry_run:
        config["target"]["config"]["dry_run"] = True

    source = loader(config["source"]["type"], config["source"]["config"])
    target = loader(config["target"]["type"], config["target"]["config"])
    comm = loader(
        config["commissioning"]["type"],
        config["commissioning"]["config"],
        module="commissioning.",
    )
    comm.set_target(target)

    if args.clean:
        target.clean()
        comm.clean(config["beamlines"])
        exit()

    start = datetime.now() - timedelta(weeks=args.start)
    end = datetime.now() + timedelta(weeks=args.end)

    logger.info(f"Replicating from {start} to {end}")

    replicator = Replicator(source, target, comm)
    replicator.run(
        config["beamlines"],
        start,
        end,
        add_defaults=config.get("add_defaults", False),
    )


if __name__ == "__main__":
    main()
