import os
from replicator.replicate import main


def test_replicate(with_database):
    os.environ["REPLICATOR_CONFIG"] = f"{os.path.dirname(__file__)}/config.yml"
    main([])
