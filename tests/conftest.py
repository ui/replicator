import os
import subprocess
import yaml
import pytest
import requests


@pytest.fixture(scope="session")
def with_database():
    with open(f"{os.path.dirname(__file__)}/config.yml") as yml:
        all_config = yaml.safe_load(yml)

        target = all_config["target"]["config"]

        host, database = target["url"].split("/")
        hostname, port = host.split(":")

        command = [
            "mysql",
            "-u",
            target["user"],
            f"-p{target['pass']}",
            "-h",
            hostname,
            "-P",
            port,
            database,
        ]

        if os.environ.get("CI"):
            print("Running in CI, not dropping database")
        else:
            print("Dropping and creating database")
            p = subprocess.run(
                command,
                input=f"DROP DATABASE {database}\n;CREATE DATABASE {database};\nexit",
                encoding="ascii",
                stdout=subprocess.PIPE,
            )

        for sql_url in [
            "https://raw.githubusercontent.com/DiamondLightSource/ispyb-database/master/schemas/ispyb/tables.sql",
        ]:
            resp = requests.get(sql_url)
            if resp.status_code != 200:
                raise Exception("Couldnt download database table description")

            with open("/tmp/sqltmp", "w") as sql_file:
                sql_file.write(resp.text)

            with open("/tmp/sqltmp", "r") as sql_file:
                print("inserting", sql_url)
                p = subprocess.Popen(
                    command,
                    stdin=sql_file,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                )
                print(p.communicate("exit\n"))
