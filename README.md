A python replicator with suport for SMIS and Mimosa, easily extendable to other database systems.

# Installation
```
conda create -n replicator
conda activate replicator
conda install --file requirements-conda.txt
pip install -e .
```

# Configuration
```
copy config-sample.yml config.yml
```

# Usage
```
# dry run
replicate --dry-run

# 4 weeks back, 4 weeks forward
replicate -s 4 -e 4
```
